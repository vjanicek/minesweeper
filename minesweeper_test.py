"""Minesweeper class tests."""

import unittest

import minesweeper


class MinesweeperTest(unittest.TestCase):

    def setUp(self):
        self.minesweeper = minesweeper.Minesweeper(size=5)

    def tearDown(self):
        self.minesweeper = None

    def test_grid(self):
        self.assertEqual(5, len(self.minesweeper.grid))
        self.assertEqual(5, len(self.minesweeper.grid[0]))

    def test_get_surroundings(self):
        positions = self.minesweeper.get_surroundings((0, 0))
        self.assertEqual([[0, 0], [0, 1], [1, 0], [1, 1]], positions)

    def test_add_mine(self):
        self.minesweeper.add_mine((0, 0))
        self.minesweeper.add_mine((0, 1))
        self.minesweeper.add_mine((1, 1))
        self.assertEqual(['M', 'M', 2, 0, 0], self.minesweeper.grid[0])
        self.assertEqual([3, 'M', 2, 0, 0], self.minesweeper.grid[1])
        self.assertEqual([1, 1, 1, 0, 0], self.minesweeper.grid[2])
        self.assertEqual([0, 0, 0, 0, 0], self.minesweeper.grid[3])
        self.assertEqual([0, 0, 0, 0, 0], self.minesweeper.grid[4])

    def test_follow_empty(self):
        self.minesweeper.add_mine((0, 0))
        self.minesweeper.add_mine((0, 1))
        self.minesweeper.add_mine((1, 1))
        whites = self.minesweeper.follow_empty([0, 4])
        self.assertEqual(16, len(whites))
        self.assertFalse(sum([self.minesweeper.grid[y][x] for y, x in whites]))


if __name__ == '__main__':
    unittest.main()
