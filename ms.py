"""Minesweeper Pygame UI.

Random mines will be put after the user discovers
the new cell. This avoids the user starting on a mine.

TODO(juan): move constants to a file.
TODO(juan): move helpers to a file.
TODO(juan): move event queye to a file.
TODO(juan): set flags on bricks. All flags on bricks could mean a won game.

Juan Hernandez, 2018.

Simple 2D excercises.
"""

import sys

import pygame
from pygame import locals as py_locals

import minesweeper


SCREEN_SIZE = (1000, 1000)
LINES = 15
LINE_THICKNESS = 2
MINE_IMG = 'img/mine.png'
BRICKS_IMG = 'img/bricks.png'
CELL_SIZE = SCREEN_SIZE[0] / LINES
MINE_BASE_SIZE = (200, 200)
# Set size dynamically depending on cell size.
MINE_LOAD_SIZE = (int(CELL_SIZE // 1.1), int(CELL_SIZE // 1.1))

_RED = (255, 0, 0)
_GREEN = (0, 255, 0)
_BLUE = (0, 0, 255)
_DARK_BLUE = (0, 0, 128)
_WHITE = (255, 255, 255)
_BLACK = (0, 0, 0)
_PINK = (255, 200, 200)

# Color map.
COLORS = {
    'red': _RED,
    'green': _GREEN,
    'blue': _BLUE,
    'dark_blue': _DARK_BLUE,
    'black': _BLACK,
    'pink': _PINK,
    'white': _WHITE
}

COLOR_WARNING = {
     '1': _BLUE,
     '2': _BLACK,
     '3': _GREEN,
     '4': _RED,
     '5': _RED,
     '6': _RED,
     '7': _RED,
     '8': _RED,
}


# Initialize pygame.
pygame.init()
pygame.font.init()
# Caption.
pygame.display.set_caption('Pysweeper')

# Set SCREEN.
SCREEN = pygame.display.set_mode(SCREEN_SIZE, 0, 32)


# Add an event queue to render stuff on every iteration.
# If not added, new events are only called on the current iteration.
# TODO(juan): serialize or find a way to keep unique tasks if needed.
class EventQueue:

    def __init__(self):
        self.stuff = {}

    def add(self, label, event, *args, **kwargs):
        """Pushes an event into the internal system."""
        self.stuff.update({label: [event, args, kwargs]})

    def remove(self, label):
        try:
            return self.stuff.pop(label)
        except KeyError:
            return

    def __contains__(self, label):
        return label in self.stuff


# Helpers.
def draw_line(screen, color, initial_coordinates,
              ending_coordinates, thickness=1, closed=False):
    """Draws a line.

    Args:
        screen: pygame SCREEN object.
        color: RGB color.
        initial_coordinates: sequence with initial point x,y coordinates.
        ending_coordinates: sequence with final point x,y coordinates and where
          line will end.
        thickness: line thickbness.
        closed: if line returns to its initial point.
    """
    pygame.draw.lines(screen, color, closed,
                      [initial_coordinates, ending_coordinates],
                      thickness)


# Functions based on cursor.
def get_cell_coordinates(mouse_coordinates):
    """Gets cell coordinates based on the cursor position.

    Args:
        mouse_coordinates: pygame.mouse.get_pos() values.
    Returns:
        tupple with (x, y) grid coordinates.
    """
    # Set mouse position to specific cell.
    mouse_x, mouse_y = mouse_coordinates
    return int(mouse_x / CELL_SIZE), int(mouse_y / CELL_SIZE)


def get_cell_center(cell_coordinates):
    """Gets the center of a given cell.

    Args:
        cell_coordinates: cell cordinates in the grid.
    Returns:
        tupple with (x, y) cell center for positioning objects in a cell.
    """
    current_x, current_y = cell_coordinates
    middle_x = (current_x * CELL_SIZE) + (CELL_SIZE / 2)
    middle_y = (current_y * CELL_SIZE) + (CELL_SIZE / 2)
    return middle_x, middle_y


def get_render_position(cell_coordinates, object_size):
    """Adapts the cell position to the exact middle using the image size.

    Args:
        cell_coordinates: cell cordinates in the grid.
        object_size: (x, y) object size in pixels to be loaded.
    Returns:
        tupple with adapted center to load objects from its middle into a cell.
    """
    cell_center = get_cell_center(cell_coordinates)
    return (cell_center[0] - object_size[0] / 2,
            cell_center[1] - object_size[1] / 2)


def draw_text(value: str, screen, color, cell_coordinates):
    """Draws text into a given coordinate.

    Args:
        value: value to be set as text.
        screen: pygame screen object.
        color: color hex value.
        cell_coordinates: cell grid coordinates.
    """
    font_size = int(CELL_SIZE / 2)
    font = pygame.font.SysFont('ubuntumono', font_size)
    text = font.render(value, True, color)
    render_position = get_render_position(
            cell_coordinates,
            # Put it in the middle.
            (font_size - font_size / 2, font_size))
    screen.blit(text, render_position)


def load_image(image_path, screen, position, new_size, alpha=True):
    """Loads a given image into a screen.

    Args:
        image_path: image path in the project.
        screen: pygame screen object where image will be rendered.
        position: (x, y) position to be placed into the screen.
        new_size: (height/width) with size adapted to the cell.
        alpha: convert image alpha.
    """
    if alpha:
        image = pygame.image.load(image_path).convert_alpha()
    else:
        image = pygame.image.load(image_path).convert()
    image = pygame.transform.scale(image, new_size)
    screen.blit(image, position)


def load_cell_image(cell_coordinates, image_path, screen, new_size,
                    alpha=True):
    """Loads a given image into a cell using cell coordinates.
        image_path: image path in the project.
        screen: pygame screen object where image will be rendered.
        position: (x, y) position to be placed into the screen.
        new_size: (height/width) with size adapted to the cell.
        alpha: convert image alpha.
    """
    position = get_render_position(cell_coordinates, MINE_LOAD_SIZE)
    load_image(image_path, screen, position, new_size, alpha)


# Main Routines.
event_queue = EventQueue()
ms = minesweeper.Minesweeper(LINES, 'hard')
clicked_cells = {}
while True:
    # Set SCREEN white.
    SCREEN.fill(COLORS['white'])

    # Call everything in the Event Queue.
    for _, (event, args, kwargs) in event_queue.stuff.items():
        event(*args, **kwargs)

    # NbyN grid. no need to set different loops for lines.
    # TODO(juan): fix for NxM grids.
    current_count = 0
    while current_count <= SCREEN_SIZE[0]:
        # Add Y lines.
        draw_line(SCREEN, COLORS['black'],
                  (0, current_count), (SCREEN_SIZE[0], current_count),
                  LINE_THICKNESS)
        # Add X lines.
        draw_line(SCREEN, COLORS['black'],
                  (current_count, 0), (current_count, SCREEN_SIZE[1]),
                  LINE_THICKNESS)
        current_count += CELL_SIZE

    # Creating game at first.
    for y in range(LINES):
        for x in range(LINES):
            value = ms.grid[y][x]
            label = '{}-{}-background'.format(x, y)
            if value and value is not minesweeper.MINE:
                # Add Number of mine surrounding cells.
                event_queue.add(label, draw_text, str(value),
                                SCREEN, COLOR_WARNING[str(value)],
                                (x, y))
            elif value == minesweeper.MINE:
                event_queue.add(label, load_cell_image,
                                (x, y), MINE_IMG, SCREEN,
                                MINE_LOAD_SIZE)

    # Cover all cells in cell that haven't been clicked.
    for y in range(LINES):
        for x in range(LINES):
            label = '{}-{}-brick'.format(x, y)
            if label not in clicked_cells:
                event_queue.add(label, load_cell_image,
                                (x, y), BRICKS_IMG, SCREEN,
                                MINE_LOAD_SIZE, alpha=False)

    # Events.
    for event in pygame.event.get():
        # Exit.
        if event.type == py_locals.QUIT:
            print('Goodbye my friends!')
            sys.exit()

        # Mouse clicks.
        if event.type == py_locals.MOUSEBUTTONDOWN:
            # Get middle point in cell.
            cell_coordinates = get_cell_coordinates(pygame.mouse.get_pos())
            x_position, y_position = cell_coordinates

            label = '{}-{}-brick'.format(x_position, y_position)
            clicked_cells.update({label: True})
            # Remove task from queue.
            event_queue.remove(label)

            # If click is white, discover all adjacent whites, also set
            # as clicked so it doesn't show up again.
            if ms.grid[y_position][x_position] == 0:
                empty_cells = ms.follow_empty((y_position, x_position))
                for y, x in empty_cells:
                    label = '{}-{}-brick'.format(x, y)
                    event_queue.remove(label)
                    clicked_cells.update({label: True})
            # If a bomb is found, discover all bricks.
            if ms.grid[y_position][x_position] == minesweeper.MINE:
                for y in range(LINES):
                    for x in range(LINES):
                        label = '{}-{}-brick'.format(x, y)
                        event_queue.remove(label)
                        clicked_cells.update({label: True})

    # Update the sreen on every iteration.
    pygame.display.update()
