"""Minesweeper libraries."""

import random


_COORDINATE_POSITIONS = [-1, 0, 1]
_DIFFICULTY_LEVELS = {
    'easy': 4,
    'medium': 3,
    'hard': 2,
}
MINE = 'M'


class Minesweeper:
    """Creates a minesweeper grid n by n given by the attribute "number".

    Properties:
        size: size that will be used in the NxN or SIZExSIZE grid.
    """

    def __init__(self, size, difficulty=None):
        self.size = size
        self.grid = self._grid()
        if difficulty:
            self._random_generator(difficulty)

    def get_surroundings(self, position):
        """Get surrounding cells from a given cell.

        Args:
            position: sequence (y, x) with grid coordinates.
        Returns:
            List with given cell surrounding coordinates.
        """
        positions = []
        y_position, x_position = position
        for y in _COORDINATE_POSITIONS:
            for x in _COORDINATE_POSITIONS:
                current_y = y_position + y
                current_x = x_position + x
                if all([current_y >= 0, current_y < self.size,
                        current_x >= 0, current_x < self.size]):
                    # if [current_y, current_x] != position:
                    positions.append([current_y, current_x])
        return positions

    def add_mine(self, position):
        """Adds a mine to a cell and updates surrounding cells.

        Args:
            position: sequence (y, x) with grid coordinates.
        Returns:
            List of modified squares or None if the cell already had a mine.
        """
        cell_y, cell_x = position
        # Check if cell doesn't have a bomb already.
        if self.grid[cell_y][cell_x] == MINE:
            return

        # If not, set mine to given position.
        self.grid[cell_y][cell_x] = MINE
        # Update surroundings.
        positions = self.get_surroundings(position)
        for y, x in positions:
            if isinstance(self.grid[y][x], int):
                self.grid[y][x] += 1
        return positions

    def follow_empty(self, position, visited=None, results=None):
        """Discovers all white spaces of a given cell.

        Returns:
            List with zeros followed by given cell.
        """
        if not visited:
            visited = []
        if not results:
            results = []
        for cell in self.get_surroundings(position):
            y, x = cell
            if cell not in visited:
                visited.append(cell)
                if not self.grid[y][x]:
                    results.append(cell)
                    self.follow_empty(cell, visited, results)
        return results

    def _random_generator(self, difficulty):
        """Creates mines based on difficulty."""
        val = int(self.size * 4 / _DIFFICULTY_LEVELS[difficulty])
        for x in range(val):
            self.add_mine(
                (random.randrange(0, self.size),
                 random.randrange(0, self.size))
            )

    def _grid(self):
        """Creates a NxN grid using self.size."""
        return [[0 for x in range(self.size)] for x in range(self.size)]
